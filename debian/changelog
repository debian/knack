knack (0.12.0-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/0.12.0'
  * d/control: bump Standards-Version to 4.7.0, no changes

 -- Luca Boccassi <bluca@debian.org>  Tue, 16 Jul 2024 13:48:49 +0100

knack (0.11.0-1) unstable; urgency=medium

  * Update upstream source from tag 'upstream/0.11.0'
  * Bump Standards-Version to 4.6.2, no changes
  * Bump copyright year ranges in d/copyright

 -- Luca Boccassi <bluca@debian.org>  Sat, 29 Jul 2023 11:08:12 +0100

knack (0.10.1-1) unstable; urgency=medium

  * Merge tag 'v0.10.1' into debian/sid

 -- Luca Boccassi <bluca@debian.org>  Mon, 05 Dec 2022 12:38:04 +0000

knack (0.10.0-1) unstable; urgency=medium

  * Merge tag 'v0.10.0' into debian/sid
  * Drop dependency on colorama, no longer needed
  * Bump Standards-Version to 4.6.1, no changes
  * Bump copyright year ranges in d/copyright

 -- Luca Boccassi <bluca@debian.org>  Tue, 06 Sep 2022 13:04:11 +0100

knack (0.9.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set field Upstream-Name in debian/copyright.

  [ Luca Boccassi ]
  * Merge tag 'v0.9.0' into debian/sid
  * Drop python3-mock dependency, no longer required
  * Bump Standards-Version to 4.6.0, no changes
  * Switch to dh-sequence-python3
  * autopkgtest: add dependency on python3-all:any
  * Skip broken test

 -- Luca Boccassi <bluca@debian.org>  Mon, 08 Nov 2021 16:10:33 +0000

knack (0.8.2-1) unstable; urgency=medium

  * Merge tag 'v0.8.2' into debian/sid
  * Add new build dependencies
  * Bump debhelper-compat to 13

 -- Luca Boccassi <bluca@debian.org>  Tue, 17 Aug 2021 12:34:38 +0100

knack (0.8.0~rc2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Luca Boccassi ]
  * Merge tag 'v0.8.0rc2' into debian/sid
  * Bump Standards-Version to 4.5.1, no changes

 -- Luca Boccassi <bluca@debian.org>  Mon, 25 Jan 2021 15:14:42 +0000

knack (0.7.2-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Luca Boccassi ]
  * New upstream release 0.7.2
  * Remove redundant Testsuite from d/control
  * Fix capitalization in description

 -- Luca Boccassi <bluca@debian.org>  Fri, 07 Aug 2020 11:42:23 +0100

knack (0.7.1-1) unstable; urgency=medium

  * Merge tag 'v0.7.1' into debian/sid

 -- Luca Boccassi <bluca@debian.org>  Tue, 19 May 2020 15:44:58 +0100

knack (0.6.3-3) unstable; urgency=medium

  * Fix watch file syntax for downloaded tarball name
  * Bump Standards-Version to 4.5.0, no changes.
  * Add autopkgtest support
  * Move package under Python Modules Team maintenance

 -- Luca Boccassi <bluca@debian.org>  Sat, 07 Mar 2020 10:50:35 +0000

knack (0.6.3-2) unstable; urgency=medium

  * Team upload.
  * Build-depend on pylint which is now python3, not pylint3

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Tue, 10 Dec 2019 16:47:18 +0100

knack (0.6.3-1) unstable; urgency=medium

  * Add -b debian/sid to Vcs-Git field.
  * Bump Standards-Version to 4.4.0.
  * Merge tag 'v0.6.3' into debian/sid

 -- Luca Boccassi <bluca@debian.org>  Thu, 12 Sep 2019 15:29:04 +0100

knack (0.6.2-1) unstable; urgency=medium

  * Merge tag 'v0.6.2' into debian/sid

 -- Luca Boccassi <bluca@debian.org>  Tue, 11 Jun 2019 12:37:04 +0100

knack (0.6.1-2) unstable; urgency=medium

  [ Chris Lamb ]
  * Do not ship temporary directories in the binary. (Closes: #928288)

 -- Luca Boccassi <bluca@debian.org>  Wed, 01 May 2019 10:53:09 +0100

knack (0.6.1-1) unstable; urgency=medium

  * New upstream release 0.6.1:
    - Fix red color contrast for error messages (#147)
    - local context: support chained config file (#148)
    - Bump the version for new release (#149)
    - Always read from local for configured_default (#150)

 -- Luca Boccassi <bluca@debian.org>  Mon, 29 Apr 2019 13:43:08 +0100

knack (0.5.4-1) unstable; urgency=medium

  * Initial release. (Closes: #925094)

 -- Luca Boccassi <bluca@debian.org>  Wed, 03 Apr 2019 23:48:02 +0100
